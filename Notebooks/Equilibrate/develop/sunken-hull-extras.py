
def eval_curv(comps, method, cross_term_inds):
    single_pt = False
    if comps.ndim==1:
        single_pt = True
        comps = comps[np.newaxis,:]

    if method=='quad':
        XiXj = comps[:, cross_term_inds[0]]*comps[:, cross_term_inds[1]]
        X2_sum = np.sum(XiXj,axis=1)
        curv_term = X2_sum
    elif method=='quad-full':
        XiXj = comps[:, cross_term_inds[0]]*comps[:, cross_term_inds[1]]
        curv_term = XiXj
    elif method=='xlogx':
        logX = np.log(comps)
        logX[comps==0] = 0
        XlogX = comps*logX
        # XlogX[comps==0] = 0
        XlogX_sum = np.sum(XlogX,axis=1)
        curv_term = XlogX_sum
    elif method=='none':
        curv_term = np.zeros((comps.shape[0],0))
    else:
        assert False, method + ' is not a valid method for eval_curv.'

    if single_pt:
        curv_term = curv_term[0]

    return curv_term


# +
# phs_sym

# +
def get_quad_inds(Nelems):
    ind_rows, ind_cols = np.tril_indices(Nelems,-1)
    cross_term_inds = np.vstack((ind_rows,ind_cols))
    return cross_term_inds

cross_term_inds = get_quad_inds(Nelems)
# cross_term_inds[0]
# -
